package lib;

/** The VolumeModel represents the model and behaviour of a volume control.
 * The volume can be increased or decreased. The range of the volume is from
 * 0 to some maximum. The volume can be muted.
 * @author lz, la */
public class VolumeModel {

    //Fields
    private int maxVolume;  // The maximum volume. Must be non-negative.
    private int volume;     //0..maxVolume
    private boolean muted;  //true=muted, false=not muted


    //Constructors
    /** Default constructor. Creates a new volume control with a volume
     * range 0..10, initial volume is 5, and mute is off.*/
    public VolumeModel() {
        this.maxVolume = 10;
        this.volume = 5;
        this.muted = false;
    }

    /** Creates a volume model with the specified maximum volume.
     * The initial volume is half of the maximum volume.
     * @param maxVolume The maximum volume must be non-negative. If a negative
     * value is supplied then the maximum volume will be set to 0.
     */
    public VolumeModel(int maxVolume) {
        if (maxVolume >= 0){
            this.maxVolume = maxVolume;
            this.volume = this.maxVolume / 2;
        } else {
            this.maxVolume = 0;
            this.volume = 0;
        }
    }

    /** Creates a volume model with the specified maximum and initial volume,
     * and muted setting.
     * @param maxVolume The maximum volume must be non-negative. If a negative
     * value is supplied then the maximum volume will be set to 0.
     * @param initialVolume The initial volume must be in the range 0 to
     * maxVolume. If is is not in this range then it is set to zero.
     * @param muted True=muted, false=not muted.
     */
    public VolumeModel(int maxVolume, int initialVolume, boolean muted) {
       if (maxVolume >= 0){
           this.maxVolume = maxVolume;
       } else {
           this.maxVolume = 0;
       }
       if (volume >= 0 && volume <= this.maxVolume){
           this.volume = initialVolume;
        } else {
           this.volume = 0;
       }
       this.muted = muted;
    }


    //Methods
    /** Sets the volume of the model to the given value.
     * @param volume The volume must be in the range 0..maximum volume,
     * otherwise the value remains unchanged.
     */
    public void setVolume(int volume) {
        if (volume >= 0 && volume <= this.maxVolume) {
            this.volume = volume;
        }
    }

    /** Increases the volume by one unit if possible. */
    public void turnUp() {
        if (this.volume < this.maxVolume - 1) {
            this.volume++;
        }
    }

    /** Increases the volume by the given amount if possible, otherwise sets
     * the volume to the maximum.
     * @param amount The amount to increase the volume by.
     */
    public void turnUp(int amount) {
       if (amount + this.volume < this.maxVolume){
            this.volume = this.volume + amount;
       } else {
           this.volume = this.maxVolume;
       }
    }

    /** Decreases the volume by one unit if possible. */
    public void turnDown() {
        if (this.volume > 1){
            this.volume--;
        }
    }

    /** Decreases the volume by the given amount if possible, otherwise sets
     * the volume to 0.
     * @param amount The amount to decrease the volume by.
     */
    public void turnDown(int amount) {
       if (amount > this.volume){
           this.volume = 0;
       } else {
           this.volume = this.volume - amount;
       }
    }

    /** Sets the muted flag according to the given value.
     * @param muted true=muted, false= not muted.
     */
    public void setMuted(boolean muted) {
       this.muted = muted;
    }

    /** Returns the maximum volume setting of the volume model.
     * @return The current maximum volume value. */
    public int getMaxVolume() {
       return this.maxVolume;
    }

    /** Returns the current volume setting.
     * @return The current volume value. */
    public int getVolume() {
      return this.volume;
    }

    /** Returns the current volume setting.
     * @return true=muted, false=not muted.
     */
    public boolean isMuted() {
        return this.muted;
    }

    /**Returns a textual representation of the volume model state.
     * @return A textual representation of the volume model. */
    public String toString() {
        return "VolumeModel:[volume=" + volume + ", maxVolume=" + maxVolume + ", muted=" + muted + "]";
    }
    
    /** Compares this VolumeModel to the specified object. The result is true if and 
	 * only if the argument is not null and is a VolumeModel object that has the same
	 * number of sides and score (i.e. face value) as this object.
	 * 
	 * @param obj the object to compare this VolumeModel against.
	 * 
	 * @return true if the given object represents a VolumeModel equivalent to this model, false otherwise.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null || this.getClass() != obj.getClass())
			return false;

		VolumeModel other = (VolumeModel) obj;

		return this.volume == other.volume && 
				this.maxVolume == other.maxVolume &&
				this.muted == other.muted;
	}
}
