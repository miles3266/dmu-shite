package main;

import lib.VolumeModel;

/**
 * Example demo program for the VolumeControl class
 * @author lz,la
 */
public class VolumeModelDemo {

    
    public static void main(String[] args) {

        //create an instance of VolumeModel using the no-argument constructor
        VolumeModel vm1 = new VolumeModel();
        System.out.println(vm1.toString());
        System.out.println("max volume: " + vm1.getMaxVolume());
        System.out.println("volume: " + vm1.getVolume());
        System.out.println("muted: " + vm1.isMuted());

        //setVolume to maximum
        vm1.setVolume(vm1.getMaxVolume());
        System.out.println(vm1.toString());

        //turnDown until zero
        while (vm1.getVolume()>0) {
            vm1.turnDown();
            System.out.println(vm1.toString());
        }

        vm1.setMuted(true);
        //turn up until max
        int max = vm1.getMaxVolume();
        while (vm1.getVolume()< max) {
            vm1.turnUp(2);
            System.out.println(vm1.toString());
        }

        //create an instance of VolumeModel using three-argument constructor
        VolumeModel vm2 = new VolumeModel(11, 8, true);
        System.out.println(vm2.toString());
        System.out.println("max volume: " + vm2.getMaxVolume());
        System.out.println("volume: " + vm2.getVolume());
        System.out.println("muted: " + vm2.isMuted());

        //test turnDown(int)
        vm2.turnDown(1);
        System.out.println(vm2.toString());
        vm2.turnDown(8);
        System.out.println(vm2.toString());
    }
}
