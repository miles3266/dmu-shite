package test;


import static org.junit.Assert.assertSame;

import org.junit.Test;

import lib.VolumeModel;

public class VolumeModelTest {
	
	/*
	/*  testing the Default constructor 
	 * 
	 * 
	 */
	@Test
	public void testDefaultConstructor() {
		VolumeModel v = new VolumeModel();
		
		assertSame("MaxVolume field should be initialised with  10", 10, v.getMaxVolume());
		assertSame("volume field should be initialised with 5", 5, v.getVolume());
		assertSame("Mute field should be initialised with false", false, v.isMuted());
	}

	
	@Test
	public void testCustomConstructor() {
		
		VolumeModel v = new VolumeModel(6);
		
		assertSame("MaxVolume field should be initialised with  6", 6, v.getMaxVolume());
		assertSame("volume field should be initialised with 3", 3, v.getVolume());
		assertSame("Mute field should be initialised with false", false, v.isMuted());		
	} 

	
	/*
	/* By testing the custom constructor with more than one set of arguments, you can ensure
	 * that the values cannot be hardcoded just to pass a given unit test. 
	 * 
	 * Running a test like this may however depend on the context and purpose of the tests.
	 */
	
	@Test
	public void testCustomConstructor2() {
	     
		VolumeModel v = new VolumeModel(20,8,true);
		
		assertSame("MaxVolume field should be initialised with  20", 20, v.getMaxVolume());
		assertSame("volume field should be initialised with 8", 8, v.getVolume());
		assertSame("Mute field should be initialised with true", true, v.isMuted());	
	} 
	
		 
	@Test
	public void testSetAndGetVolume() {
		VolumeModel v = new VolumeModel(20,8,true);
		v.setVolume(11);				
		assertSame("volume field should be  11", 11, v.getVolume());
	}

	@Test
	public void testSetAndGetVolume1() {
		VolumeModel v = new VolumeModel(20,8,true);
		v.setVolume(-9);				
		assertSame("volume field should be  8", 8, v.getVolume());
	}
	
	@Test
	public void testturnUp() {
		VolumeModel v = new VolumeModel(20,8,false);
		v.turnUp();				
		assertSame("volume field should be  9", 9, v.getVolume());
	}
	
	@Test
	public void testturnUp1() {
		VolumeModel v = new VolumeModel(20,20,false);
		v.turnUp();				
		assertSame("volume field should be  20", 20, v.getVolume());
	}
	
	@Test
	public void testturnUp2() {
		VolumeModel v = new VolumeModel(20,8,false);
		v.turnUp(3);				
		assertSame("volume field should be  11", 11, v.getVolume());
	}
	
	@Test
	public void testturnUp3() {
		VolumeModel v = new VolumeModel(20,10,false);
		v.turnUp(30);				
		assertSame("volume field should be  20", 20, v.getVolume());
	}
	
	@Test
	public void testturnDown() {
		VolumeModel v = new VolumeModel(20,8,false);
		v.turnDown(3);				
		assertSame("volume field should be  5", 5, v.getVolume());
	}
	
	@Test
	public void testturnDown1() {
		VolumeModel v = new VolumeModel(20,10,false);
		v.turnDown(30);				
		assertSame("volume field should be  0", 0, v.getVolume());
	}
	
	@Test
	public void testSetandGetMuted() {
		VolumeModel v = new VolumeModel(20,10,false);
		v.setMuted(true);			
		assertSame("volume field should be  true", true, v.isMuted());
	}
	
	
}
